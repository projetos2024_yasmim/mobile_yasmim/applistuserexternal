import React from "react";
import { View, FlatList, Text} from "react-native";


const Details = ({route})=> {
    const {user} = route.params;
    
        return(
            <View>
                <Text>Detalhes Da Tarefas</Text>
                <Text>Nome: {user.name}</Text>
                <Text>Email: {user.email}</Text>
                <Text>Telefone: {user.phone}</Text>
                <Text>Cidade: {user.address.city}</Text>
                <Text>Username: {user.username}</Text>
                <Text>Casa: {user.address.suite}</Text>
                <Text>Rua: {user.address.street}</Text>
                <Text>Site: {user.website}</Text>
                <Text>Nome da companhia: {user.company.name}</Text>
                <Text>Nome de usuário:{user.username}</Text>
                </View>
            
        )
    }
    export default Details;
