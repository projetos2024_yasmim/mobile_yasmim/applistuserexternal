import React, { useEffect, useState } from "react";
import { View, FlatList, TouchableOpacity, Text } from "react-native";
import axios from "axios";

const List = ({ navigation }) => {
    const [users, setUsers] = useState([]); 
    useEffect(() => {
    getUsers();
    }, []);



async function getUsers(){
    try{
       const response = await axios.get("https://jsonplaceholder.typicode.com/users");
        setUsers(response.data)
    }catch(error){
    console.log(error);
    }
}

    const userPress = (user) => {
        navigation.navigate("DetalhesDosUsuários", {user});
    };

return (
    <View>
        <Text>Lista de usuários</Text>
        <FlatList
            data={users}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (
                <View>
                    <TouchableOpacity onPress={()=> userPress(item)}>
                        <Text style={{ margimBottom: 10, fontSize: 20,}}>{item.name}</Text>
                    </TouchableOpacity> 
                </View>                   
                
            )}
        />
        </View>
    
)
}

export default List;