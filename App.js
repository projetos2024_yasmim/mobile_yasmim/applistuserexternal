import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import List from './src/list';
import Details from './src/details';

const Stack = createStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="ListaDeUsuários">
      <Stack.Screen name="Lista De Usuários" component={List}/>
      <Stack.Screen name="DetalhesDosUsuários" component={Details} options= {{title:'Detalhes'}} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}



